# Challenge Interbanking

*Quibar Manuel*

## Tecnologia

Java 17

Spring Boot 3.0.6

OpenApi

## SWAGGER

http://localhost:8080/challenge/interbanking/swagger-ui/index.html

## Endpoints

### POST: `/transferencia`
##### body
    {
        "cuitEmpresa": "string",
        "importe": 0,
        "ctaDebito": "string",
        "ctaCredito": "string"
    }

##### Return: `201 Created`

### POST: `/empresa`
##### body
    {
        "cuit": "string",
        "razonSocial": "string"
    }

##### Return: `200 OK`
    {
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "cuit": "string",
        "razonSocial": "string",
        "fechaAdhesion": "2023-05-13",
        "eliminada": true
    }


### GET: `/empresa/transferencia`

##### Return: `200 OK`
    [
        {
            "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "cuit": "string",
            "razonSocial": "string",
            "fechaAdhesion": "2023-05-13",
            "eliminada": true
        }
    ]


### GET: `/empresa/adhesion`

##### Return: `200 OK`
    [
        {
            "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "cuit": "string",
            "razonSocial": "string",
            "fechaAdhesion": "2023-05-13",
            "eliminada": true
        }
    ]
