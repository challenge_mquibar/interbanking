package ar.quibar.challenge.interbanking.domain.model;

import java.time.LocalDate;
import java.util.UUID;

import org.hibernate.annotations.UuidGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Empresa {

	@Id
    @GeneratedValue
    @UuidGenerator
	private UUID id;
	private String cuit;
	private String razonSocial;
	
	@Temporal(TemporalType.DATE)
	private LocalDate fechaAdhesion;
	private boolean eliminada;
}
