package ar.quibar.challenge.interbanking.domain.port;

import java.time.LocalDateTime;
import java.util.List;

import ar.quibar.challenge.interbanking.domain.model.Transferencia;

public interface TransferenciaDao {

	Transferencia save (Transferencia transferencia);
	
	List<Transferencia> findByStampGreaterThanEqual(LocalDateTime stamp);
}
