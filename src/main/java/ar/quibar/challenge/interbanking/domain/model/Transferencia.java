package ar.quibar.challenge.interbanking.domain.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import org.hibernate.annotations.UuidGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Transferencia {

	@Id
    @GeneratedValue
    @UuidGenerator
	private UUID id;
	private BigDecimal importe;
	@ManyToOne
	@JoinColumn(name = "idempresa", referencedColumnName = "id")
	private Empresa empresa;
	private String ctaDebito;
	private String ctaCredito;
	@Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime stamp;
}
