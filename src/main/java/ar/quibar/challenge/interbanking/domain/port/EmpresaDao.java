package ar.quibar.challenge.interbanking.domain.port;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import ar.quibar.challenge.interbanking.domain.model.Empresa;

public interface EmpresaDao {

	Empresa save(Empresa empresa);
	
	List<Empresa> findByFechaAdhesionGreaterThanEqual(LocalDate fechaAdhesion);
	
	Optional<Empresa> findByCuit(String cuit);
}
