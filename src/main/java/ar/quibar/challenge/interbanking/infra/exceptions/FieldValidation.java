package ar.quibar.challenge.interbanking.infra.exceptions;

import lombok.Data;

@Data
public class FieldValidation {

	private String name;
	private String msg;
	public FieldValidation(String name, String msg) {
		this.name = name;
		this.msg = msg;
	}
	
	
}
