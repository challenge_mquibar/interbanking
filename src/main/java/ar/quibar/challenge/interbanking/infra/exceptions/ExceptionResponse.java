package ar.quibar.challenge.interbanking.infra.exceptions;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class ExceptionResponse {

	private String message;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private final LocalDateTime datetime;
	private List<FieldValidation> fields;
	
	public ExceptionResponse() {
		this(null,null);
	}
	
	public ExceptionResponse(String message) {
		this(message,null);
	}
	
	public ExceptionResponse(String message, List<FieldValidation> fields) {
		this.message = message;
		datetime = LocalDateTime.now(ZoneId.of("America/Buenos_Aires"));
		this.fields = fields;
	}
}
