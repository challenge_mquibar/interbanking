package ar.quibar.challenge.interbanking.infra.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.quibar.challenge.interbanking.domain.model.Empresa;
import ar.quibar.challenge.interbanking.domain.port.EmpresaDao;

public interface EmpresaRepository extends JpaRepository<Empresa, UUID>, EmpresaDao {

}
