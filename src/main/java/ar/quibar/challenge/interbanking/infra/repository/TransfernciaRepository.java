package ar.quibar.challenge.interbanking.infra.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.quibar.challenge.interbanking.domain.model.Transferencia;
import ar.quibar.challenge.interbanking.domain.port.TransferenciaDao;

public interface TransfernciaRepository extends JpaRepository<Transferencia, UUID>, TransferenciaDao {

}
