package ar.quibar.challenge.interbanking.infra.controller;

import static org.springframework.http.ResponseEntity.created;

import java.net.URI;

import org.apache.logging.log4j.util.Strings;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.quibar.challenge.interbanking.application.dto.CreateTransferenciaDto;
import ar.quibar.challenge.interbanking.application.usecase.CrearTransferenciaUseCase;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("transferencia")
@RequiredArgsConstructor
public class TransferenciaController {

	private final CrearTransferenciaUseCase crearTransferencia;
	
	@PostMapping
	public ResponseEntity<Void> create(@RequestBody @Valid CreateTransferenciaDto body){
		crearTransferencia.execute(body);
		return created(URI.create(Strings.EMPTY)).build();
	}
}
