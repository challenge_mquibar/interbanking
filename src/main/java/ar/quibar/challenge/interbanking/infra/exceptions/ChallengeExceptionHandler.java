package ar.quibar.challenge.interbanking.infra.exceptions;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
@Configuration
public class ChallengeExceptionHandler extends ResponseEntityExceptionHandler {
	
	private static final String INVALID_FIELDS = "Invalid Fields";
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleException(Exception ex, HttpHeaders headers, WebRequest request){
		log.error("Generic Exception: {}",ex.getMessage());
		final var response = new ExceptionResponse(ex.getMessage());
		return new ResponseEntity<>(response,headers,HttpStatus.BAD_REQUEST);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {

		List<FieldValidation> fields = ex.getBindingResult()
				.getFieldErrors()
				.stream()
				.map(field -> new FieldValidation(field.getField(), field.getDefaultMessage()))
				.collect(Collectors.toList());
				
		final var response = new ExceptionResponse(INVALID_FIELDS,fields);

		return new ResponseEntity<>(response, headers,status);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public final ResponseEntity<ExceptionResponse> handleValidationCsvFileException(RuntimeException ex,  WebRequest request){
		log.error("Se produjo una Exception: {}", ex.getMessage());
		final var response = new ExceptionResponse(ex.getMessage());
		return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
	}
	
	
}
