package ar.quibar.challenge.interbanking.infra.controller;

import static org.springframework.http.ResponseEntity.ok;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.quibar.challenge.interbanking.application.dto.CreateEmpresaDto;
import ar.quibar.challenge.interbanking.application.usecase.BuscarEmpresaPorTransaccionUseCase;
import ar.quibar.challenge.interbanking.application.usecase.BuscarPorAdhesionUseCase;
import ar.quibar.challenge.interbanking.application.usecase.CrearEmpresaUseCase;
import ar.quibar.challenge.interbanking.domain.model.Empresa;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("empresa")
@RequiredArgsConstructor
public class EmpresaController {
	
	private final CrearEmpresaUseCase crearEmpresa;
	private final BuscarPorAdhesionUseCase buscarPorAdhesion;
	private final BuscarEmpresaPorTransaccionUseCase buscarPorTransaccion;
	
	@PostMapping
	public ResponseEntity<Empresa> create(@RequestBody @Valid CreateEmpresaDto body){
		return ok(crearEmpresa.execute(body));
	}
	
	@GetMapping("adhesion")
	public ResponseEntity<List<Empresa>> findByAdhesion(){
		return ok(buscarPorAdhesion.execute());
	}
	
	@GetMapping("transferencia")
	public ResponseEntity<List<Empresa>> findByTransferencia(){
		return ok(buscarPorTransaccion.execute());
	}

}
