package ar.quibar.challenge.interbanking.application.dto;

import jakarta.validation.constraints.NotBlank;

public record CreateEmpresaDto(@NotBlank String cuit, @NotBlank String razonSocial) {

}
