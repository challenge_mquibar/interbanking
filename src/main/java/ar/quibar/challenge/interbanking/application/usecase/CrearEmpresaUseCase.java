package ar.quibar.challenge.interbanking.application.usecase;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import ar.quibar.challenge.interbanking.application.dto.CreateEmpresaDto;
import ar.quibar.challenge.interbanking.domain.model.Empresa;
import ar.quibar.challenge.interbanking.domain.port.EmpresaDao;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class CrearEmpresaUseCase {
	
	private final EmpresaDao dao;

	public Empresa execute(CreateEmpresaDto dto){
		
		if (dao.findByCuit(dto.cuit())
			.isPresent()){
			throw new RuntimeException(String.format("El cuit %s ya se encuentra registrado en el sistema", dto.cuit()));
		}
		
		return dao.save(Empresa.builder()
				.cuit(dto.cuit())
				.razonSocial(dto.razonSocial())
				.fechaAdhesion(LocalDate.now())
				.build());
		
	}
}
