package ar.quibar.challenge.interbanking.application.usecase;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import ar.quibar.challenge.interbanking.application.dto.CreateTransferenciaDto;
import ar.quibar.challenge.interbanking.domain.model.Transferencia;
import ar.quibar.challenge.interbanking.domain.port.EmpresaDao;
import ar.quibar.challenge.interbanking.domain.port.TransferenciaDao;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class CrearTransferenciaUseCase {

	private final TransferenciaDao dao;
	private final EmpresaDao empresaDao;
	
	public void execute(CreateTransferenciaDto dto) {
		var empresa = empresaDao.findByCuit(dto.cuitEmpresa())
				.orElseThrow(() -> new RuntimeException("Empresa no encontrada"));
		var trx = Transferencia.builder()
				.empresa(empresa)
				.importe(dto.importe())
				.ctaCredito(dto.ctaCredito())
				.ctaDebito(dto.ctaDebito())
				.stamp(LocalDateTime.now())
				.build();
		dao.save(trx);
		
	}
	
}
