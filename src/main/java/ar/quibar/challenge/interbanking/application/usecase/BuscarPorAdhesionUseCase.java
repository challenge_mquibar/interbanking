package ar.quibar.challenge.interbanking.application.usecase;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Component;

import ar.quibar.challenge.interbanking.domain.model.Empresa;
import ar.quibar.challenge.interbanking.domain.port.EmpresaDao;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class BuscarPorAdhesionUseCase {

	private final EmpresaDao dao;
	
	public List<Empresa> execute(){
		return dao.findByFechaAdhesionGreaterThanEqual(LocalDate.now().minusMonths(1L));
	}
}
