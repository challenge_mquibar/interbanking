package ar.quibar.challenge.interbanking.application.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

public record CreateTransferenciaDto(
		@NotBlank String cuitEmpresa,
		@NotNull @Positive BigDecimal importe, 
		@NotBlank String ctaDebito,
		@NotBlank String ctaCredito) {

}
