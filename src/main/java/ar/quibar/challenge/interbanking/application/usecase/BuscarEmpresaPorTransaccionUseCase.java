package ar.quibar.challenge.interbanking.application.usecase;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import ar.quibar.challenge.interbanking.domain.model.Empresa;
import ar.quibar.challenge.interbanking.domain.model.Transferencia;
import ar.quibar.challenge.interbanking.domain.port.TransferenciaDao;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class BuscarEmpresaPorTransaccionUseCase {

	private final TransferenciaDao dao;
	
	public List<Empresa> execute(){
		var fechaDesde = LocalDate.now().minusMonths(-1L);
		return dao.findByStampGreaterThanEqual(fechaDesde.atStartOfDay())
			.stream()
			.map(Transferencia::getEmpresa)
			.distinct()
			.collect(Collectors.toList());
	}
}
